# Wardrobify

Team:


 Bea Klein - Shoes!
 Xander Clemens - Hats

## Design
    We wanted a sleek design, nice and minimalistic.
    We wanted all the pages to be easily accessed from our home page via multiple dropdowns in the navigation bar.

## Shoes microservice

In the shoes microservice, I included fields for the name of the shoe, the brand (manufacturer), the color of the shoe,
a picture of the shoe, and the bin to which the shoe belongs. The Shoe Model is connected to the Wardrobe API via a Bin
Value Object. 

## Hats microservice

For models we used the following features: color, fabric. style_name, picture_url. We tied this to our Hat.


For models we used the following features: shelf_number, closet_name, section_number and import_href. We tied this to our Location Value Object.
