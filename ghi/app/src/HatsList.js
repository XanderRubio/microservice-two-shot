import React from 'react';

function HatsList(props) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {props.hats.map(hat => {
          return (
            <tr key={hat.href}>
              <td>{ hat.name }</td>
              <td>{ hat.picture_url }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatsList;
