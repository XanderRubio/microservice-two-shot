import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList'
import ShoeForm from './ShoeForm'
import HatsForm from './HatsForm'
import HatsList from './HatsList'



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="all" element={<ShoeList />} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
            <Route path="hats">
              <Route path="all" element={<HatsList />} />
              <Route path="new" element={<HatsForm />} />
            </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
